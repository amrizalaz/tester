package com.amrizal.example.firebasetester;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.amrizal.example.firebasetester.volley.VolleyHelper;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class MainActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = MainActivity.class.getSimpleName();
    private boolean loginGoogle = false;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private GoogleApiClient mGoogleApiClient;
    private TextView googleUsername;
    private View progress;
    private ImageView googlePhoto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        progress = findViewById(R.id.progress_layout);

        Log.d(TAG, "Log this");
        initFirebase();
        initGoogleLogin();

        TextView textView = (TextView) findViewById(R.id.line_with_text);
        textView.setText(getResources().getString(R.string.app_name));
    }

    public void onDownloadAsset(View view) {
        FirebaseStorage storage = FirebaseStorage.getInstance();
        //StorageReference storageRef = storage.getReferenceFromUrl("gs://project-2041291700268874188.appspot.com/images/tulips.jpg");

        String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        Log.d(TAG, "uid-" + uid);
        StorageReference storageRef = FirebaseStorage.getInstance().getReference().child("images/Tulips.jpg");
        File localFile = null;
        try {
            localFile = File.createTempFile("images", ".jpg");
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        progress.setVisibility(View.VISIBLE);
        final File finalLocalFile = localFile;
        storageRef.getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                Log.d(TAG, "Downloaded to-" + finalLocalFile.getAbsolutePath() + " [" + String.valueOf(taskSnapshot.getTotalByteCount()) + "]");
                progress.setVisibility(View.GONE);
                finalLocalFile.delete();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                Log.d(TAG, exception.getLocalizedMessage());
                Toast.makeText(getBaseContext(), exception.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                progress.setVisibility(View.GONE);
            }
        });
    }

    private void initGoogleLogin() {
        googleUsername = (TextView) findViewById(R.id.google_credential);
        googlePhoto = (ImageView) findViewById(R.id.google_photo);
        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        // Build a GoogleApiClient with access to the Google Sign-In API and the
// options specified by gso.
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }

    private void initFirebase() {
        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    loginGoogle = true;
                    googleUsername.setText(user.getEmail());
                    Uri photoUri = user.getPhotoUrl();
                    VolleyHelper.getInstance(getBaseContext()).getImageLoader().get(String.valueOf(photoUri), new ImageLoader.ImageListener() {
                        @Override
                        public void onResponse(ImageLoader.ImageContainer imageContainer, boolean b) {
                            Bitmap bitmap = imageContainer.getBitmap();
                            googlePhoto.setImageBitmap(bitmap);
                        }

                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            googlePhoto.setImageBitmap(null);
                        }
                    });
                    /*if(photoUri != null){
                        googlePhoto.setImageUrl(String.valueOf(photoUri), MyApplication.getInstance().getImageLoader()); //ImgController from your code.
                    }else{
                        googlePhoto.setImageBitmap(null);
                    }*/

                    Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
                } else {
                    loginGoogle = false;
                    googleUsername.setText("");
                    googlePhoto.setImageBitmap(null);
                    // User is signed out
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
                progress.setVisibility(View.GONE);
                // ...
            }
        };
    }

    public void onLoginGoogle(View view) {
        progress.setVisibility(View.VISIBLE);
        if(loginGoogle){
            //
            Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        FirebaseAuth.getInstance().signOut();
                        if(status.isSuccess()){
                            Toast.makeText(getBaseContext(), "Logged out", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

        }else{
            Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
            startActivityForResult(signInIntent, REQUEST.GOOGLE_SIGN_IN);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == REQUEST.GOOGLE_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleGoogleSignInResult(result);
        }
    }

    private void handleGoogleSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleGoogleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            GoogleSignInAccount acct = result.getSignInAccount();
            firebaseAuthWithGoogle(acct);
        } else {
            loginGoogle = false;
            googleUsername.setText("");
            // Signed out, show unauthenticated UI.
        }
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);

        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "signInWithCredential:onComplete:" + task.isSuccessful());

                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "signInWithCredential", task.getException());
                            Toast.makeText(getBaseContext(), "Authentication failed.", Toast.LENGTH_SHORT).show();
                        }
                        // ...
                    }
                });
    }

    public void onLoginFacebook(View view) {
    }

    public void onDownloadZip(View view) {

        String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        Log.d(TAG, "uid-" + uid);
        StorageReference storageRef = FirebaseStorage.getInstance().getReference().child("zip/ic_launcher.zip");
        File localFile = null;
        try {
            localFile = File.createTempFile("zip-", ".zip");
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        progress.setVisibility(View.VISIBLE);
        final File finalLocalFile = localFile;
        storageRef.getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                Log.d(TAG, "Downloaded to-" + finalLocalFile.getAbsolutePath() + " [" + String.valueOf(taskSnapshot.getTotalByteCount()) + "]");
                try {
                    List<String> result = ZipHelper.unzipFile(finalLocalFile.getAbsolutePath(), getApplicationInfo().dataDir);
                    Toast.makeText(getBaseContext(), "Unzipped " + result.size() + " file(s)", Toast.LENGTH_LONG).show();
                    progress.setVisibility(View.GONE);
                } catch (IOException e) {
                    progress.setVisibility(View.GONE);
                    e.printStackTrace();
                }

                finalLocalFile.delete();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                Log.d(TAG, exception.getLocalizedMessage());
                Toast.makeText(getBaseContext(), exception.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                progress.setVisibility(View.GONE);
            }
        });
    }

    private class REQUEST {
        public static final int GOOGLE_SIGN_IN = 1000;
    }
}
