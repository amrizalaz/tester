package com.amrizal.example.firebasetester;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Created by amrizal.zainuddin on 31/10/2016.
 */
public class ZipHelper {
    public static List<String> unzipFile(String targetZip, String destinationFolder) throws IOException {

        InputStream is;
        ZipInputStream zis;

        String filename;
        is = new FileInputStream(targetZip);
        zis = new ZipInputStream(new BufferedInputStream(is));
        ZipEntry ze;
        byte[] buffer = new byte[1024];
        int count;

        List<String> result = new ArrayList<>();
        while ((ze = zis.getNextEntry()) != null)
        {
            filename = ze.getName();

            String destinationPath = destinationFolder + "/" + filename;
            File fmd = new File(destinationPath);

            if (ze.isDirectory()) {
                fmd.mkdirs();
                continue;
            }else{
                fmd = new File(fmd.getParent());
                fmd.mkdirs();
            }

            FileOutputStream fout = new FileOutputStream(destinationPath);

            while ((count = zis.read(buffer)) != -1)
            {
                fout.write(buffer, 0, count);
            }

            fout.close();
            zis.closeEntry();

            result.add(destinationPath);
        }

        zis.close();
        return result;
    }
}
