package jp.co.smbc.gridbeacon;

/**
 * Created by amrizal.zainuddin on 12/8/2016.
 */
public class BeaconConstants {
    public static final String CLOUDID = "cloudid";
    public static final String FAILURE_MESSAGE = "failure_message";

    public static final String EVENT_RESULT = "event_result";
    public static final int EVENT_FAILED = 0;
    public static final int EVENT_SUCCESS = 1;
    public static final String GRID_URL = "url";
    public static final String GUID = "guid";
    public static final String HASH_SEED = "hashseed";
    public static final String ENCRYPTION_SEED = "encseed";

    // http type
    public final static int GET = 0;
    public final static int POST = 1;
    public final static int PUT = 2;
    public static final int DELETE = 3;
    public static final String GID = "gid";
    public static final String VDID = "vdid";
    public static final String URL = "url";
    public static final String S = "s";
}
